#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <ctime>
#include <list> 
#include <vector>
#include "Pokemon.h"
#include <windows.h>
using namespace std;

int getRandGen(int i, int j) {
	return rand() % i + j;
}
//--------------------------------------INTRO---------------------------------------------------------------------
void displayGame() {
	cout << "	,-.----." << endl;
	cout << "    \t\\    /  \\                  ,-.                    ____" << endl;
	cout << "    \t|   :    \\             ,--/ /|                  ,'  , `." << endl;
		cout << "    \t|   |  .\\ :   ,---.  ,--. :/ |               ,-+-,.' _ |   ,---.        ,---," << endl;
		cout << "   \t.   :  |: |  '   ,'\\ :  : ' /             ,-+-. ;   , ||  '   ,'\   ,-+-. /  |" << endl;
		cout << "   \t|   |   \\ : /   /   ||  '  /      ,---.  ,--.'|'   |  || /   /   | ,--.'|'   |" << endl;
		cout << "   \t|   : .   /.   ; ,. :'  |  :     /     \\|   |  ,', |  |,.   ; ,. :|   |  ,\"' |" << endl;
		cout << "    \t;   | |`-' '   | |: :|  |   \\   /    /  |   | /  | |--' '   | |: :|   | /  | |" << endl;
		cout << "    \t|   | ;    '   | .; :'  : |. \\ .    ' / |   : |  | ,    '   | .; :|   | |  | |" << endl;
		cout << "    \t:   ' |    |   :    ||  | ' \ \\'   ;   /|   : |  |/      |   :    ||   | |  |/" << endl;
		cout << "    \t:   : :     \\   \\  / '  : |--' '   |  / |   | |`-'       \\   \\  / |   | |--'" << endl;
		cout << "    \t|   | :      `----'  ;  |,'    |   :    |   ;/            `----'  |   |/" << endl;
		cout << "   \t`---'.|              '--'       \\   \\  /'---'                     '---'" << endl;
		cout << "     .--.--. `---`                ____      `----'     ,--,                  ___" << endl;
			cout << "     /  /    '.   ,--,           ,'  , `.             ,--.'|                ,--.'|_" << endl;
			cout << "    |  :  /`. / ,--.'|        ,-+-,.' _ |        ,--, |  | :                |  | :,'   ,---.    __  ,-." << endl;
			cout << "    ;  |  |--`  |  |,      ,-+-. ;   , ||      ,'_ /| :  : '                :  : ' :  '   ,'\ ,' ,'/  /|" << endl;
			cout << "    |  :  ;_    `--'_     ,--.'|'   |  || .--. |  | : |  ' |     ,--.--.  .;__,'  /  /   /   |'  | |' |" << endl;
			cout << "     \\  \\    `. ,' ,'|   |   |  ,', |  |,'_ /| :  . | '  | |    /       \\ |  |   |  .   ; ,. :|  |   ,'" << endl;
			cout << "      `----.   \'  | |   |   | /  | |--'|  ' | |  . . |  | :   .--.  .-. |:__,'| :  '   | |: :'  :  /" << endl;
			cout << "      __ \\  \\  ||  | :   |   : |  | ,   |  | ' |  | | '  : |__  \\__\\/: . .  '  : |__'   | .; :|  | '" << endl;
			cout << "     /  /`--'  /'  : |__ |   : |  |/    :  | : ;  ; | |  | '.'| ,\" .--.; |  |  | '.'|   :    |;  : |" << endl;
			cout << "    '--'.     / |  | '.'||   | |`-'     '  :  `--'   \\;  :    ;/  /  ,.  |  ;  :    ;\\   \\  / |  , ;" << endl;
			cout << "      `--'---'  ;  :    ;|   ;/         :  ,      .-./|  ,   /;  :   .'   \\ |  ,   /  `----'   ---'" << endl;
			cout << "                |  ,   / '---'           `--`----'     ---`-' |  ,     .-./  ---`-'" << endl;
			cout << "                 ---`-'                                        `--`---'             A BASPRG2 Project" << endl;
	cout << "Press \"Enter\" to start\n";
	cin.ignore();
	system("cls");

}
void getName(string name) {
	cout << "Hi, trainer! What is your name?\n";
	cout << ">";
	getline(cin, name);
	cin.ignore();
	system("cls");
}
//------------------------------------ADVENTURE------------------------------------------------------------------
void showStatusPokemons(vector<Pokemon*> &pokemons) {
	for (int i = 0; i < pokemons.size(); i++) {
		 pokemons[i]->displayStats();
	}
	system("pause");
	system("cls");
}
int checkLocation(int x, int y) {
	if ((y > 4 || y < -4)) {
		//Unknown Location
		return 2;
	}
	else if (x < -4 || x > 4) {
		//Unknown Location
		return 2;
	}
	else if ((x <3 || x > -3) && (y > 2 || y < -2)) {
		//nRoute 1
		return 1;
	}
	else if ((x <3 || x > -3) && (y < 3 || y > -3)) {
		//Pallet Town
		return 0;
	}
	else {
		return 0;
	}
}
void movePlayer(int x, int y) {
	char move;
	int location;
	string currentLocation[5] = { "Pallet Town", "Route 1", "Unknown Location" , "Viridian City", "Pewter City" };
	cout << "Where do you want to move?\n";
	cout << "[W] - Up        [S] - Down      [A] - Left      [D] - Right\n\n";
	cout << ">";
	cin >> move;
	switch (move) {
	case 'w':
		y = y + 1;
		break;
	case 's':
		y = y - 1;
		break;
	case 'a':
		x = x - 1;
		break;
	case 'd':
		x = x + 1;
		break;
	}

	cout << "You are now at (" << x << ", " << y << ")\n";
	location = checkLocation(x, y);
	cout << "You are in " << currentLocation[location] << endl;
	cin.ignore();
}
bool doHealPokemon(vector<Pokemon*> pokemons) {
	char healPokemon;

	cout << "Welcome to the Pallet Town Pokemon Center!\n";
	cin.ignore();
	cout << "Would you like me to heal your Pokemon back to perfect health? (y/n)\n";
	cout << ">";
	cin >> healPokemon;
	if (healPokemon == 'y') {
		cout << "*ten ten tenenenen*\n";
		system("pause");
		for (int i = 0; i < pokemons.size(); i++) {
			pokemons[i]->hp = pokemons[i]->baseHp;
		}
		cout << "Your Pokemons are now in perfect health\n\n";
		showStatusPokemons(pokemons);
		cout << "\nWe hope to see you again!\n\n";
		system("pause");
		system("cls");
		return true;
	}
	else if (healPokemon == 'n'){
		cout << "\nWe hope to see you again!\n\n";
		return false;
	}
}
//-------------------------------------COMBAT-----------------------------------------------------------------------
bool catchPokemon(vector<Pokemon*> pokemons, Pokemon *enemy) {
	int randomCaught;
	cout << "You threw a Pokeball!\n";
	cout << ".\n";
	Sleep(600);
	cout << "..\n";
	Sleep(600);
	cout << "...\n";
	Sleep(600);
	randomCaught = getRandGen(100, 1);
	if (randomCaught > 90) {
		cout << enemy->name << " got out of the ball!\n\n";
		return true;
	}
	else {
		cout << "Successfully caught " << enemy->name << "!\n\n";
		pokemons.push_back(enemy);
		enemy->displayStats();

		cout << "MY CURRENT POKEMON SIZE: " << pokemons.size();
		showStatusPokemons(pokemons);
		return false;
	}
	system("pause");
	system("cls");
}
//MAIN PROGRAM-------------------------------------------------------------------------------
int main() {
	srand(time(NULL));
	int x = 0;
	int y = 0;

	int choose;
	bool noPokemonCenter;
	bool noItemShop;

	char move;
	int location = 0;
	string currentLocation[5] = { "Pallet Town", "Route 1", "Route 2" , "Viridian City", "Pewter City" };

	int playerDo;
	int pokemonEncounter;

	int remaining = 0;
	bool win = true;

	string name;
	//-----------------------------------------------------------
	displayGame();
	getName(name);

	cout << "I have three(3) Pokemons here.\nYou can have one! Go on, choose!\n\n";
	string startingPokemon[3] = { "Bulbasaur \t Grass Type \t Level 5",
		"Charmander \t Fire Type \t Level 5","Squirtle \t Water Type \t Level 5" };
	for (int i = 0; i < 3; i++) {
		cout << i + 1 << " - " << startingPokemon[i] << endl;
	}
	cout << "\n\nInput choice: ";
	cin >> choose;

	Pokemon *pokemon = new Pokemon(choose);
	vector<Pokemon*> pokemons;

	pokemons.push_back(pokemon);
	cout << "You chose " << startingPokemon[choose - 1] << "!\n\n\n";
	system("pause");
	system("cls");
	cout << "Your journey begins now!\n";
	system("pause");
	system("cls");

	//Pokemon *enemy = new Pokemon();
	//vector<Pokemon*> enemy;
	//START JOURNEY-------------------------------------------------------------------------------------------
	do {
		int moveCombat;
		bool stay = true;
		bool isDead = false;
		bool canFight = true;
		bool gotHeal = false;

		do {
			noPokemonCenter = false;
			noItemShop = false;
			cout << "What would you like to do?\n";
			cout << "[1] - Move \n[2] - Check Status \n[3] - Inventory\n";
			if (location == 0) {
				cout << "[4] - Item Shop \n[5] - Pokemon Center\n";
			}
			cout << ">";
			cin >> playerDo;
			if ((location != 0)) {
				cout << "\nYou are currerntly outside the town...\n";
				if ((playerDo == 4)) {
					cout << "Please go to the nearest town to visit the item shop.\n\n";
					noItemShop = true;
				}
				else if ((playerDo == 5)) {
					cout << "Please go to the nearest town to heal your pokemon.\n\n";
					noPokemonCenter = true;
				}
			}		
		} while (noPokemonCenter);
		system("cls");
		switch (playerDo) {
		case 1:
			cout << "Where do you want to move?\n";
			cout << "[W] - Up        [S] - Down      [A] - Left      [D] - Right\n\n";
			cout << ">";
			cin >> move;
			switch (move) {
			case 'w':
				y = y + 1;
				break;
			case 's':
				y = y - 1;
				break;
			case 'a':
				x = x - 1;
				break;
			case 'd':
				x = x + 1;
				break;
			}
			cout << "You are now at (" << x << ", " << y << ")\n";
			location = checkLocation(x, y);
			cout << "You are in " << currentLocation[location] << endl;
			//Combat Encounter Chance
			if (location > 0) {
				pokemonEncounter = getRandGen(100, 1);
				if (pokemonEncounter > 20) {
					//COMBAT-----------------------------------------------------------------------------------------
					Pokemon *enemy = new Pokemon();
					vector<Pokemon*> enemies;
					//int tempRemaining = 0;

					cout << "You have encountered a wild " << enemy->name << "!\n\n";
					enemy->displayStats();
					do {
						//Checker----------------------------------------------------------------
					/*	cout << "\nPOKEMON CURRENT SIZE: " << pokemons.size() << endl;
						cout << "Pokemons[Remaining] : " << remaining << endl;*/
						//-----------------------------------------------------------------------
						if (remaining == pokemons.size()) {
							cout << "You are out of pokemon...\n";
							cout << "You can no longer fight " << enemy->name << endl;
							canFight = false;
						}
						cout << "\nWhat would you like to do ?" << endl;
						if (canFight == true) {
							cout << "1 - Battle\n";
						}
						cout << "2 - Catch\n3 - Use item\n4 - Check Status\n5 - Run away\n";
						cout << ">";
						cin >> moveCombat;
						system("pause");
						system("cls");
						switch (moveCombat) {
						case 1:
							//Pokemon Attack-----------------------
							if (isDead != true) {
								pokemons.at(remaining)->attack(enemy);
								enemy->isDead(pokemons[remaining]);
							}
							if (pokemons[remaining]->isDead(enemy) == true) {
								isDead = true;
								stay = false;
								pokemons.at(remaining)->levelUp();
								pokemons.at(remaining)->evolution();
								remaining = 0;
								break;
							}
							//Enemy Attack-----------------------
							if (isDead != true) {
								enemy->attack(pokemons.at(remaining));
							}
							if (enemy->isDead(pokemons.at(remaining)) == true) {
								isDead = true;
								stay = false;
								remaining = remaining + 1;
								//return remaining;
								break;
							}
							break;
						case 2:
							if (pokemons.at(remaining)->pokeBalls != 0) {
								stay = catchPokemon(pokemons, enemy);
								canFight = true;
								if (stay == false) {
									remaining = 1;
								}
							}
							else{
								cout << "\nYou don't have any pokeballs left.\n";
							}
							break;
						case 3:
							pokemons.at(remaining)->inventory();
							pokemons.at(remaining)->useItem();
							break;
						case 4:
							pokemons.at(remaining)->displayStats();
							break;
						case 5:
							cout << "Ran away safely!\n";
							stay = false;
							break;
						};
					} while ((enemy->hp != 0) && stay);
				}
			}
			system("pause");
			system("cls");
			break;
		case 2:
			showStatusPokemons(pokemons);
			system("pause");
			system("cls");
			break;
		case 3:
			pokemons.at(remaining)->inventory();
			break;
		case 4:
			if (noItemShop != true) {
				pokemons.at(remaining)->itemShop();
			}
			break;
		case 5:
			gotHeal = doHealPokemon(pokemons);
			if (gotHeal == true) {
				remaining = 0;
			}
			break;
		}
	} while (win);
	system("pause");
	return 0;
}
